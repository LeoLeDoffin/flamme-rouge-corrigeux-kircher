import static org.junit.Assert.*;

import org.junit.Test;


public class CoureurTest {

	@Test
	public void testChangerPosition() {
		Rouleur r = new Rouleur(1);
		
		r.changerPosition(5);
		
		assertEquals("Sa position devrait etre 6", 6, r.getPos());
	}

}
