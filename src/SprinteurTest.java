import static org.junit.Assert.*;

import org.junit.Test;


public class SprinteurTest {

	@Test
	
	public void testConstructeur() {
		Sprinteur s = new Sprinteur(1);
		assertEquals("L equipe du sprinteur devrait etre", 1, s.getEquipe());
		assertEquals("La position du sprinteur devrait etre", 1, s.getPos());
		assertEquals("L'aspiration du sprinteur devrait etre", false, s.getEstAspire());
	}
	

}
