import java.io.Serializable;

public class Rouleur extends Coureur implements Serializable{
	

	/**
	 * Constructeur de la classe Rouleur
	 * @param e
	 */
	public Rouleur(int e){
		super(e);
	}
	
	/**
	 * Methode toString qui retourne les caract�ristique du rouleur 
	 * @return
	 */
	public String toString(){
		return "Le Rouleur de l'�quipe" + this.getEquipe() + " se trouve � la case " + this.getPos() ;
	}
}
