import java.io.Serializable;
import java.util.ArrayList;


public class Sprinteur extends Coureur implements Fatigable, Serializable{
	
	/**
	 * Constructeur de la classe Sprinteur
	 * @param e
	 */
	public Sprinteur(int e){
		super(e);
	}
	
	/**
	 * Methode permetant de v�rifier si un sprinteur est fatigue ou non
	 * @param lCoureurs
	 * @return
	 */
	public boolean estFatigue(ArrayList<Coureur> LCoureurs) {
		boolean estFatigue = false;
		for(Coureur coureur : LCoureurs){
			if(coureur.getPos() > this.getPos()+2){
				estFatigue = true;
			}
		}
		return estFatigue;
	}
	/**
	 * Methode toString qui retourne les caract�ristique du sprinteur 
	 * @return
	 */
	public String toString(){
		return "Le Sprinteur de l'�quipe" + this.getEquipe() + " se trouve � la case " + this.getPos() ;
	}
}
