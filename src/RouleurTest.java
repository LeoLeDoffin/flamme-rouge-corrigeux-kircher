import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RouleurTest {

	@Test
	public void testConstructeur() {
		Rouleur r = new Rouleur(1);
		assertEquals("L equipe du rouleur devrait etre", 1, r.getEquipe());
		assertEquals("La position du rouleur devrait etre", 1, r.getPos());
		assertEquals("L du rouleur devrait etre", false, r.getEstAspire());
	}

}
