import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class Jeu {

	public Jeu() throws IOException {

		// Creation de la liste des coureurs
		ArrayList<Coureur> lCoureurs;
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez indiquer le nombre de joueur.");
		int nbJoueurs = sc.nextInt();
		lCoureurs = new ArrayList<Coureur>();
		for (int i = 0; i < nbJoueurs; i++) {
			Rouleur r = new Rouleur(i + 1);
			Sprinteur s = new Sprinteur(i + 1);
			lCoureurs.add(r);
			lCoureurs.add(s);
		}

		// Creation du circuit
		System.out.println("Veuillez indiquer la longueur du circuit.");
		int taille = sc.nextInt();
		boolean fini = false;
		int tour = 1;
		// Phase de Jeu
		while (!fini) {
			
			System.out.println("Tour " + tour + " :");
			for (Coureur c : lCoureurs) {
				int de = c.lanceDe();
				if (c instanceof Sprinteur) {
					if (((Sprinteur) c).estFatigue(lCoureurs)) {
						de -= 1;
					}
					if (c.getEstAspire())
						de += 1;
				}
				c.changerPosition(de);
				System.out.println(c.toString());
				if (c.getPos() >= taille)
					fini = true;

			}

			// Phase d'aspiration
			Collections.sort(lCoureurs, new ComparateurClassement());
			for (Coureur c2 : lCoureurs) {
				c2.estAspire(lCoureurs);
				if (c2.getEstAspire()) {
					c2.changerPosition(1);
				}
			}
			
			System.out.println("----------------------------------------------------");
			tour++;

		}
		System.out.println("Classement de la course :");
		Collections.sort(lCoureurs);
		int compte = 1;
		for (Coureur c3 : lCoureurs) {
			if (c3 instanceof Rouleur)
				System.out.println(compte + " Rouleur de l'equipe " + c3.getEquipe());
			else
				System.out.println(compte + " Sprinteur de l'equipe " + c3.getEquipe());
			compte++;
		}
		System.out.println("\nVeuillez indiquer un nom du fichier pour enregistrer le classement: ");
		String nom = sc.next()+".txt";
		this.save(nom, lCoureurs);
		System.out.println("Le classement de la partie a �t� enregistr� dans le fichier: " + nom);
		

	}

	public void save(String nom, ArrayList<Coureur> lCoureurs) throws IOException {
		String s= "";
		try{
			
			BufferedWriter fw =new BufferedWriter(new FileWriter(nom));
			int compte = 1;
			fw.write("Classement de la partie: "+nom);
			fw.newLine();
			for (int i = 0;i<lCoureurs.size();i++) {
				if (lCoureurs.get(i) instanceof Rouleur)
					s = compte + ": Rouleur de l'equipe: " + lCoureurs.get(i).getEquipe();
				else
					s = compte + ": Sprinteur de l'equipe: " + lCoureurs.get(i).getEquipe();
				
				fw.write(s);
				fw.newLine();
				compte++;
				
			}
			
			fw.close();
		}catch (IOException e){
			e.printStackTrace();
		}
		

	}
	
	public static void chargeList(String nom) throws IOException, ClassNotFoundException {
		
		
				
				BufferedReader br = new BufferedReader(new FileReader(nom));
			      String s = br.readLine();
			      while(s != null){
			    	  System.out.println(s);
			    	  s = br.readLine();
			      }
			      
			      br.close();
		   }
	
	      
		
	
}
