import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		Scanner sc = new Scanner(System.in);
		String nom = "";
		int n;
		boolean jouer = false;

		while (!jouer) {
			System.out.println("Tapez 1 pour jouer, 2 pour charger un classement: ");
			n = sc.nextInt();
			switch (n) {
			case 1:
				new Jeu();
				break;
			case 2:
				System.out.println("Veuillez indiquer le nom du fichier (sans le .txt): ");
				try {
					nom = sc.next() + ".txt";
					Jeu.chargeList(nom);
				} catch (FileNotFoundException e) {
					System.out.println("Le nom du fichier n'extiste pas.");
					nom = sc.nextLine();
				}
				break;
			default:
				System.out.println("Saisie incorrecte.");
			}

			System.out.println("Voulez vous quitter ? 1 pour oui, 0 pour non");
			n = sc.nextInt();
			if (n == 1)
				jouer = true;

		}

	}

}
