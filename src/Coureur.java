import java.io.Serializable;
import java.util.ArrayList;



public abstract class Coureur implements Comparable<Coureur>, Serializable{
		
	private int equipe;
	protected int position;
	private boolean estAspire;
		
	/**
	 * Constructeur de la classe qui initialise l equipe, la position et le booleen estAspire
	 * @param e
	 */	
	public Coureur(int e){
		this.equipe = e;
		this.position = 1;
		this.estAspire = false;
	}
	
	/**
	 * Methode permettant de retourner un entier en tre 1 et 6
	 * @return
	 */
	public int lanceDe(){
		return (int) (Math.random()*6)+1;
	}
	
	/**
	 * Methode permettant d'actualiser la position du coureur
	 * @param d
	 */
	public void changerPosition(int d){
		this.position += d;
	}
		
	/**
	 * getter de l'attribut position
	 * @return
	 */
	public int getPos(){
		return this.position;
	}
	
	/**
	 * Methode qui permet de v�rifier si le coureur est aspire ou non
	 * @param lCoureurs
	 */
	public void estAspire(ArrayList<Coureur> lCoureurs){
		for(Coureur coureur : lCoureurs){
			if(coureur.getPos() == this.getPos()+2){
				this.estAspire = true;
			}
			if(estAspire)
				break;
		}
	}
	
	/**
	 * Getter de l attribut estAspire
	 * @return
	 */
	public boolean getEstAspire(){
		return estAspire;
	}
		
	
	/**
	 * Getter de l'attribut equipe
	 * @return
	 */
	public int getEquipe(){
		return equipe;
	}
	
	
	
	@Override
	public int compareTo(Coureur o){
		int res;
		if(this.getPos() > o.getPos()){
			res = -1;
		}else{
			res = 1;
		}
			
		return res;
	}
	
	
}

